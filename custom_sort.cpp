#include<bits/stdc++.h>
using namespace std;

class Freq{
    public:
    char value;
    int count;
};

int main()
{
    string s;
    cin >> s;
    Freq f[26];
    for(char i=0; i<26; i++)
    {
        f[i].value = char(i + 'a');
        f[i].count = 0;
    }
    for(int i=0;i<26;i++)
    {
        cout << f[i].value << " " << f[i].count << endl;
    }
    return 0;
}